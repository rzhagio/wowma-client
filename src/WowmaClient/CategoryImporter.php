<?php
namespace Rrrz\WowmaClient;

class CategoryImporter {
    function readSimple($path) {
        $book = \PHPExcel_IOFactory::load($path);

        $book->setActiveSheetIndex(0);
        $sheet = $book->getActiveSheet();

        $categories = [];
        $i = 2;
        while ($sheet->getCellByColumnAndRow(0, $i)->getValue()) {
            $categories[] = [
                'id' => intval($sheet->getCellByColumnAndRow(0, $i)->getValue()),
                'h1' => $sheet->getCellByColumnAndRow(1, $i)->getValue(),
                'h2' => $sheet->getCellByColumnAndRow(2, $i)->getValue(),
                'h3' => $sheet->getCellByColumnAndRow(3, $i)->getValue(),
                'h4' => $sheet->getCellByColumnAndRow(4, $i)->getValue(),
                'full' => $sheet->getCellByColumnAndRow(5, $i)->getValue(),
            ];
            $i++;
        }

        return $categories;
    }
}