<?php

namespace Rrrz\WowmaClient;

class WowmaClient
{

    public function searchItemInfos($shop_id, $api_key, $start = 1)
    {

        $curl = curl_init();
        $header = [
            "Authorization:Bearer " . $api_key,
            'Content-type: application/x-www-form-urlencoded'
        ];
        $params = [
            'shopId' => $shop_id,
            'startCount' => $start,
            'totalCount' => 100
        ];
        $url = 'https://api.manager.wowma.jp/wmshopapi/searchItemInfos?' . http_build_query($params);

        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => $header,
        ]);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }

    public function updateItemInfos($shop_id, $api_key, array $infos)
    {
        $root = '<?xml version="1.0" encoding="UTF-8" ?><request></request>';
        $xml = new \SimpleXMLElement($root);
        $xml->addChild('shopId', $shop_id);

        foreach ($infos as $info) {
            $info_tag = $xml->addChild('updateItemInfo');
            $info_tag->addChild('itemCode', $info['itemCode']);
            if (isset($info['itemPrice'])) {
                $info_tag->addChild('itemPrice', $info['itemPrice']);
            }
            if (isset($info['saleStatus'])) {
                $info_tag->addChild('saleStatus', $info['saleStatus']);
            }
        }

        $curl = curl_init();
        $header = [
            "Authorization:Bearer " . $api_key,
            'Content-type: application/xml; charset=utf-8'
        ];
        $url = 'https://api.manager.wowma.jp/wmshopapi/updateItemInfos';
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $xml->asXML()
        ]);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }

    public function updateStock($shop_id, $api_key, array $items)
    {
        $root = '<?xml version="1.0" encoding="UTF-8" ?><request></request>';
        $xml = new \SimpleXMLElement($root);
        $xml->addChild('shopId', $shop_id);

        foreach ($items as $item) {
            $info_tag = $xml->addChild('stockUpdateItem');
            $info_tag->addChild('itemCode', $item['itemCode']);
            if (isset($item['stockCount'])) {
                $info_tag->addChild('stockCount', $item['stockCount']);
            }
            if (isset($item['stockSegment'])) {
                $info_tag->addChild('stockSegment', $item['stockSegment']);
            }
        }

        $curl = curl_init();
        $header = [
            "Authorization:Bearer " . $api_key,
            'Content-type: application/xml; charset=utf-8'
        ];
        $url = 'https://api.manager.wowma.jp/wmshopapi/updateStock';
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $xml->asXML()
        ]);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }

    public function registerItemInfo($shop_id, $api_key, array $item)
    {
        $root = '<?xml version="1.0" encoding="UTF-8" ?><request></request>';
        $xml = new \SimpleXMLElement($root);
        $xml->addChild('shopId', $shop_id);
        $register_item = $xml->addChild('registerItem');
        $register_item->addChild('itemName', $item['itemName']);
        $register_item->addChild('itemCode', $item['itemCode']);
        $register_item->addChild('itemPrice', $item['itemPrice']);
        $register_item->addChild('taxSegment', $item['taxSegment']);
        $register_item->addChild('postageSegment', $item['postageSegment']);
        $register_item->addChild('description', $item['description']);
        $register_item->addChild('categoryId', $item['categoryId']);
        $register_item->addChild('saleStatus', $item['saleStatus']);
        if (isset($item['detailTitle'])) {
            $register_item->addChild('detailTitle', $item['detailTitle']);
        }
        if (isset($item['detailDescription'])) {
            $register_item->addChild('detailDescription', $item['detailDescription']);
        }
        $register_stock = $xml->addChild('registerStock');
        $register_stock->addChild('stockSegment', $item['stockSegment']);
        $register_stock->addChild('stockCount', $item['stockCount']);

        if (isset($item['images'])) {
            $i = 1;
            foreach ($item['images'] as $image) {
                $image_tag = $register_item->addChild('images');
                if (isset($image['imageUrl'])) {
                    $image_tag->addChild('imageUrl', $image['imageUrl']);
                }
                $image_tag->addChild('imageSeq', $i);
                $i++;
            }
        }

        $curl = curl_init();
        $header = [
            "Authorization:Bearer " . $api_key,
            'Content-type: application/xml; charset=utf-8'
        ];
        $url = 'https://api.manager.wowma.jp/wmshopapi/registerItemInfo';
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $xml->asXML()
        ]);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }

    function searchItemInfo($shop_id, $api_key, $item_code)
    {
        $curl = curl_init();
        $header = [
            "Authorization:Bearer " . $api_key,
            'Content-type: application/x-www-form-urlencoded'
        ];
        $params = [
            'shopId' => $shop_id,
            'itemCode' => $item_code
        ];
        $url = 'https://api.manager.wowma.jp/wmshopapi/searchItemInfo?' . http_build_query($params);

        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => $header,
        ]);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }

    function updateItemInfo($shop_id, $api_key, array $item)
    {
        $root = '<?xml version="1.0" encoding="UTF-8" ?><request></request>';
        $xml = new \SimpleXMLElement($root);
        $xml->addChild('shopId', $shop_id);
        $updateItem = $xml->addChild('updateItem');
        $updateItem->addChild('itemName', $item['itemName']);
        $updateItem->addChild('itemCode', $item['itemCode']);
        $updateItem->addChild('itemPrice', $item['itemPrice']);
        $updateItem->addChild('taxSegment', $item['taxSegment']);
        $updateItem->addChild('postageSegment', $item['postageSegment']);
        $updateItem->addChild('description', $item['description']);
        $updateItem->addChild('categoryId', $item['categoryId']);
        $updateItem->addChild('saleStatus', $item['saleStatus']);
        if (isset($item['detailTitle'])) {
            $updateItem->addChild('detailTitle', $item['detailTitle']);
        }
        if (isset($item['detailDescription'])) {
            $updateItem->addChild('detailDescription', $item['detailDescription']);
        }
//        $register_stock = $xml->addChild('registerStock');
//        $register_stock->addChild('stockSegment', $item['stockSegment']);
//        $register_stock->addChild('stockCount', $item['stockCount']);

        if (isset($item['images'])) {
            $i = 1;
            foreach ($item['images'] as $image) {
                $image_tag = $updateItem->addChild('images');
                if (isset($image['imageUrl'])) {
                    $image_tag->addChild('imageUrl', $image['imageUrl']);
                }
                $image_tag->addChild('imageSeq', $i);
                $i++;
            }
        }

        $curl = curl_init();
        $header = [
            "Authorization:Bearer " . $api_key,
            'Content-type: application/xml; charset=utf-8'
        ];
        $url = 'https://api.manager.wowma.jp/wmshopapi/updateItemInfo';
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $xml->asXML()
        ]);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }
}
