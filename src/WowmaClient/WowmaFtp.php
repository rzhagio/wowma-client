<?php

namespace Rrrz\WowmaClient;


class WowmaFtp
{
    protected $connection;
    protected $ftp_server;
    protected $ftp_user;
    protected $ftp_pass;
    protected $remote_files;

    function __construct($host, $user, $pass)
    {
        $this->ftp_server = $host;
        $this->ftp_user = $user;
        $this->ftp_pass = $pass;
    }

    function connect()
    {
        $this->connection = ftp_connect($this->ftp_server);
        ftp_login($this->connection, $this->ftp_user, $this->ftp_pass);
        ftp_pasv($this->connection, true);
    }

    function upload($remote, $path)
    {
        return ftp_put($this->connection, $remote, $path, FTP_BINARY, false);
    }

    function getFileList()
    {
        $this->remote_files = ftp_nlist($this->connection, '/');
        return $this->remote_files;
    }

    function isFinished($remote)
    {
        return !in_array($remote, $this->remote_files);
    }

    function disconnect()
    {
        ftp_close($this->connection);
    }
}